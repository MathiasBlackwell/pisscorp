const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require ('cors');
const port = 3000;
const path = require('path');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());
app.use(express.static('public'));

app.get('/',(req, res)=>{
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.listen(port, ()=>{
console.log(`Server started on port ${port}`)
});